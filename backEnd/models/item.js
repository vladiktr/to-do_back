const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const itemSchema = new Schema({
  title: { type: String, required: true, trim: true },
  description: { type: String, required: true, trim: true },
  author: { type: String, required: true, trim: true },
});

const Item = mongoose.model('Item', itemSchema);

module.exports = { Item };