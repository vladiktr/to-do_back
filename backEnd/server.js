const { ApolloServer } = require('apollo-server');
const connectDb = require('../config/db');
const typeDefs = require('./types/Item');
const resolvers = require('./resolvers');
const models = require('./models');
const config = require('config');

connectDb();

const server = new ApolloServer({
  typeDefs,
  resolvers,
  context: { models },
});

server.listen({ port: config.port || 8000 }).then(({ url }) => {
  console.log(`🚀 Server ready at ${url}`);
});
