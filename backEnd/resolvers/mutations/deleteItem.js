const { ApolloError } = require('apollo-server');

module.exports = async (_, { id }, { models }) => {
  const deleteItem = await models.Item.deleteOne({ _id: id });

  if (deleteItem.deletedCount) {
    return { id: id };
  } else {
    throw new ApolloError(`Failed to delete address.`);
  }
};
