const { ApolloError } = require('apollo-server');

module.exports = async (_, { id, input }, { models }) => {
  try {
    const itemToUpdate = await models.Item.findOne({ _id: id });

    if (!itemToUpdate) throw new ApolloError(`Could not find item with id: '${id}'.`, 400);

    Object.keys(input).forEach((value) => {
      itemToUpdate[value] = input[value];
    });

    const updatedItem = await itemToUpdate.save();

    return updatedItem;
  } catch (e) {
    throw new ApolloError(e);
  }
};
