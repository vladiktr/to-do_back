const { ApolloError } = require('apollo-server');

module.exports = async (_, { input }, { models }) => {
  try {
    const newItem = await models.Item.create(input);
    return newItem;
  } catch (e) {
    throw new ApolloError(e);
  }
};
