const { gql } = require('apollo-server');

module.exports = gql`
  type Item {
    id: ID!
    title: String!
    description: String!
    author: String!
  }

  input AddItemInput {
    title: String!
    description: String!
    author: String!
  }

  input UpdateItemInput {
    id: ID!
    title: String!
    description: String!
  }

  type DeleteItem {
    id: ID!
  }

  type Query {
    items(author: String!): [Item]
  }

  type Mutation {
    addItem(input: AddItemInput!): Item!
    updateItem(id: ID!, input: UpdateItemInput!): Item!
    deleteItem(id: ID!): Item!
  }
`;
